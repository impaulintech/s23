course_booking

db.user.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com",
	},
	courses: ["css", "javascript", "python"],
	department: "none"
});

db.user.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com",
		},
		courses: ["python", "react", "php"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com",
		},
		courses: ["react", "laravel", "sass"],
		department: "none"
}]);

db.user.find({ firstName: "Stephen" })

db.user.updateOne(
	{ _id : ObjectId("61d6cb5e4cc68fd8e136847b")},
	{
		$set: {
			firstName: "x",
			lastName: "x",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@gmail.com",
			},
			courses: ["python", "react", "php"],
			department: "none"
		}
	}
	);

db.user.updateMany(
	{department:"none"},
	{
		$set:{department: "HR"}
	}
	);

db.user.replaceOne(
	{firstName: "xxxx"},
	{
		$set:{department: "Operations"}
	}
	);